source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.3'

gem 'rails', github: 'rails/rails', branch: 'master'
gem 'dotenv-rails', '~> 2.5'

# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use development version of Webpacker
gem 'webpacker', github: 'rails/webpacker'

# Add SCSS support
gem 'sass-rails', '~> 5.0', '>= 5.0.7'

# Reset browser styles
gem 'normalize-rails', '~> 4.1', '>= 4.1.1'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 3.8', '>= 3.8.1'
end

group :development do
  gem "capistrano", "~> 3.10", require: false
  gem "capistrano-rails", "~> 1.4", require: false
  gem 'capistrano-bundler'
  gem 'capistrano-rbenv'
  gem 'capistrano-yarn'
  gem 'pry-rails', '~> 0.3.8'
  gem 'web-console', github: 'rails/web-console'
  gem 'listen', '>= 3.0.5', '< 3.2'
end
