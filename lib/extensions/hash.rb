class Hash
  def sample(n = 1)
    Hash[to_a.sample(n)]
  end

  def flat_join_keys(prefix: nil, separator: '-', memo: {})
    each do |key, value|
      joined_key = [prefix, key].compact.join(separator).underscore.to_sym

      if value.respond_to?(:flat_join_keys)
        value.flat_join_keys(prefix: joined_key, memo: memo)
      else
        memo[joined_key] = value
      end
    end

    memo
  end
end
