class ThemeRouteConstraint
  def matches?(request)
    params = request.params
    organization = params[:organization]
    app_name = params[:app_name]
    return true unless (organization || app_name)

    Theme.find_by(organization: organization, app_name: app_name).present?
  end
end
