require 'rails_helper'

RSpec.describe Luminance, type: :model do
  subject(:luminance) { Luminance.new(red, green, blue) }
  let(:red) { 0 }
  let(:green) { 123 }
  let(:blue) { 255 }

  describe '#new' do
    shared_examples 'not all values between 0 and 255' do
      it 'raises an error' do
        expect { luminance }.to raise_error(ActiveModel::ValidationError, error_message)
      end
    end

    context 'all values between 0 and 255' do
      it 'returns an instance of Luminance' do
        expect(luminance).to be_instance_of(Luminance)
      end
    end

    context 'value below 0' do
      let(:red) { -1 }
      let(:error_message) { 'Validation failed: Red must be between 0 and 255' }

      it_behaves_like 'not all values between 0 and 255'
    end

    context 'value above 255' do
      let(:blue) { 256 }
      let(:error_message) { 'Validation failed: Blue must be between 0 and 255' }

      it_behaves_like 'not all values between 0 and 255'
    end
  end

  describe '#calculate' do
    it 'calculates the luminance of the given RGB values' do
      expect(luminance.calculate).to eq(0.2138591773492754)
    end
  end
end
