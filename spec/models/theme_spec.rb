require 'rails_helper'

RSpec.describe Theme, type: :model do
  before do
    stub_const(
      'Theme::ORGANIZATION_COLORS',
      {
        org1: 'ff0000',
        org2: {
          app1: '00ff00',
          app2: '0000ff',
        },
      }
    )

    stub_const(
      'Theme::DEFAULT_COLORS',
      {
        def1: 'AAAAAA',
        def2: 'BBBBBB',
      }
    )
  end

  shared_examples 'a valid organization/app pair' do
    describe 'color' do
      it 'instantiates Color::Hex with correct attributes' do
        expect(Color::Hex).to receive(:new).with(expected_color_hex_value).and_call_original

        subject.color
      end
    end

    describe '#present?' do
      it 'returns true' do
        expect(theme.present?).to be(true)
      end
    end
  end

  shared_examples 'an invalid organization/app pair' do
    describe '#color' do
      before { allow(Theme::DEFAULT_COLORS).to receive(:sample).and_return({ a: '123456' }) }

      it 'instantiates Color::Hex with a default' do
        expect(Color::Hex).to receive(:new).with('123456').and_call_original

        subject.color
      end

      it 'shuffles default colors' do
        expect(Theme::DEFAULT_COLORS).to receive(:sample).and_call_original

        subject.color
      end
    end

    describe '#present?' do
      it 'returns false' do
        expect(theme.present?).to be(false)
      end
    end
  end

  context 'instance specs' do
    subject(:theme) { described_class.new(organization: organization, app_name: app_name) }
    let(:organization) { nil }
    let(:app_name) { nil }

    context 'existing theme' do
      context 'organization with no app' do
        let(:organization) { 'org1' }
        let(:expected_color_name) { 'org1' }
        let(:expected_color_hex_value) { 'ff0000' }

        it_behaves_like 'a valid organization/app pair'
      end

      context 'organization with app' do
        let(:organization) { 'org2' }
        let(:app_name) { 'app2' }
        let(:expected_color_name) { 'org2-app2' }
        let(:expected_color_hex_value) { '0000ff' }

        it_behaves_like 'a valid organization/app pair'
      end
    end

    context 'non-existent theme' do
      context 'no organization or app' do
        it_behaves_like 'an invalid organization/app pair'
      end

      context 'organization with no app' do
        let(:organization) { 'org3' }

        it_behaves_like 'an invalid organization/app pair'
      end

      context 'organization with app' do
        let(:organization) { 'org3' }
        let(:app_name) { 'app3' }

        it_behaves_like 'an invalid organization/app pair'
      end
    end
  end

  context 'class specs' do
    describe '.find_by' do
      it 'calls new with given args' do
        expect(Theme).to receive(:new).with(organization: :a, app_name: :b)

        Theme.find_by(organization: :a, app_name: :b)
      end
    end

    describe '.all' do
      before do
        allow(Theme).to receive(:organization_colors).and_return([:x,:y,:z])
        allow(Theme).to receive(:default_colors).and_return([:a,:b,:c])
      end

      it 'concatenates Theme.organization_colors and Theme.default_colors' do
        expect(Theme.all).to eq([:x, :y, :z, :a, :b, :c])
      end
    end

    describe '.organization_colors' do
      it 'returns array of Theme::NamedColor instances with correct attributes' do
        expect(Theme.organization_colors).to all(be_a(Theme::NamedColor))
        expect(Theme.organization_colors.first).to(
          have_attributes(name: 'org1', hex: 'ff0000')
        )
        expect(Theme.organization_colors.second).to(
          have_attributes(name: 'org2-app1', hex: '00ff00')
        )
        expect(Theme.organization_colors.third).to(
          have_attributes(name: 'org2-app2', hex: '0000ff')
        )
      end
    end

    describe '.default_colors' do
      it 'returns array of Theme::NamedColor instances with correct attributes' do
        expect(Theme.default_colors).to all(be_a(Theme::NamedColor))
        expect(Theme.default_colors.first).to have_attributes(name: 'def1', hex: 'AAAAAA')
        expect(Theme.default_colors.second).to have_attributes(name: 'def2', hex: 'BBBBBB')
      end
    end
  end
end
