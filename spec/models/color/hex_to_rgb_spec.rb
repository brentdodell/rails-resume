require 'rails_helper'

RSpec.describe Color::HexToRGB, type: :model do
  let(:hex) { Color::Hex.new('a74f9b') }

  shared_examples 'it converts Hex instance to RGB instance' do
    it 'instantiates RGB with correct integer values' do
      expect(Color::RGB).to receive(:new).with(167, 79, 155)
      hex_to_rgb
    end
  end

  describe '.call' do
    subject(:hex_to_rgb) { described_class.(hex) }

    it_behaves_like 'it converts Hex instance to RGB instance'
  end

  describe '#rgb' do
    subject(:hex_to_rgb) { described_class.new(hex).rgb }

    it_behaves_like 'it converts Hex instance to RGB instance'
  end
end
