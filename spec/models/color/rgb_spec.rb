require 'rails_helper'

RSpec.describe Color::RGB, type: :model do
  subject(:rgb) { Color::RGB.new(167, 79, 155) }

  describe '#to_a' do
    it 'returns array of [red, green, blue] values' do
      expect(rgb.to_a).to eq([167, 79, 155])
    end
  end

  describe '#contrast_color' do
    let(:luminance_double) { instance_double(Luminance, calculate: 0.5) }

    it 'instantiates Luminance and calls calculate' do
      expect(Luminance).to receive(:new).with(167, 79, 155).and_return(luminance_double)
      expect(luminance_double).to receive(:calculate)

      rgb.contrast_color
    end
  end

  describe '#to_hsl' do
    it 'calls RGBToHSL with self' do
      expect(Color::RGBToHSL).to receive(:call).with(rgb)

      rgb.to_hsl
    end
  end

  describe '#to_hex' do
    it 'calls RGBToHex with self' do
      expect(Color::RGBToHex).to receive(:call).with(rgb)

      rgb.to_hex
    end
  end
end
