require 'rails_helper'

RSpec.describe Color::Hex, type: :model do
  subject(:hex) { Color::Hex.new('a74f9b') }

  describe '#to_a' do
    it 'returns array of hex values for each channel [red, green, blue]' do
      expect(hex.to_a).to eq(['a7', '4f', '9b'])
    end
  end

  describe '#to_rgb' do
    it 'calls HexToRGB with self' do
      expect(Color::HexToRGB).to receive(:call).with(hex)
      hex.to_rgb
    end
  end
end
