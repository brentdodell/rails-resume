require 'rails_helper'

RSpec.describe Color::RGBToHex, type: :model do
  let(:rgb) { Color::RGB.new(167, 79, 155) }

  shared_examples 'it converts RGB instance to Hex instance' do
    it 'instantiates Hex with correct hex value' do
      expect(Color::Hex).to receive(:new).with('a74f9b')
      rgb_to_hex
    end
  end

  describe '.call' do
    subject(:rgb_to_hex) { described_class.(rgb) }

    it_behaves_like 'it converts RGB instance to Hex instance'
  end

  describe '#rgb' do
    subject(:rgb_to_hex) { described_class.new(rgb).hex }

    it_behaves_like 'it converts RGB instance to Hex instance'
  end
end
