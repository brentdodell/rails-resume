require 'rails_helper'

RSpec.describe Color::HSL, type: :model do
  subject(:hsl) { Color::HSL.new(308, 0.357, 0.482) }

  describe '#to_a' do
    it 'returns array of [red, green, blue] values' do
      expect(hsl.to_a).to eq([308, 0.357, 0.482])
    end
  end

  describe '#to_rgb' do
    it 'calls HSLToRGB with self' do
      expect(Color::HSLToRGB).to receive(:call).with(hsl)
      hsl.to_rgb
    end
  end

  describe '#split_complementary' do
    subject(:split_complementary) { hsl.split_complementary }

    it 'instatiates two new HSLs, 150 degrees in either direction' do
      expect(split_complementary[0]).to(
        have_attributes(hue: 98, saturation: 0.357, lightness: 0.482)
      )
      expect(split_complementary[1]).to(
        have_attributes(hue: 158, saturation: 0.357, lightness: 0.482)
      )
    end
  end
end
