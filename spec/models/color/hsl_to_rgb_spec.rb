require 'rails_helper'

RSpec.describe Color::HSLToRGB, type: :model do
  let(:hsl) { Color::HSL.new(308, 0.357, 0.482) }

  shared_examples 'it converts HSL instance to RGB instance' do
    it 'instantiates RGB with correct integer values' do
      expect(Color::RGB).to receive(:new).with(167, 79, 167)
      hsl_to_rgb
    end
  end

  describe '.call' do
    subject(:hsl_to_rgb) { described_class.(hsl) }

    it_behaves_like 'it converts HSL instance to RGB instance'
  end

  describe '#rgb' do
    subject(:hsl_to_rgb) { described_class.new(hsl).rgb }

    it_behaves_like 'it converts HSL instance to RGB instance'
  end
end
