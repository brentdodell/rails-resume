require 'rails_helper'

RSpec.describe Color::RGBToHSL, type: :model do
  let(:rgb) { Color::RGB.new(167, 79, 155) }

  shared_examples 'it converts RGB instance to HSL instance' do
    it 'instantiates HSL with correct values' do
      expect(Color::HSL).to(
        receive(:new).with(
          308.1818181818182,
          0.35772357723577236,
          0.4823529411764706
        )
      )

      rgb_to_hsl
    end
  end

  describe '.call' do
    subject(:rgb_to_hsl) { described_class.(rgb) }

    it_behaves_like 'it converts RGB instance to HSL instance'
  end

  describe '#rgb' do
    subject(:rgb_to_hsl) { described_class.new(rgb).hsl }

    it_behaves_like 'it converts RGB instance to HSL instance'
  end
end
