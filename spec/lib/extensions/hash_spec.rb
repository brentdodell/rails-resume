require 'rails_helper'

RSpec.describe Hash do
  subject(:hash) do
    {
      level1_a: {
        level2_a: 'hello',
        level2_b: 'world',
      },
      level1_b: 'hi!'
    }
  end

  describe "#flat_join_keys" do
    subject(:hash_with_flat_joined_keys) { hash.flat_join_keys }
    let(:expected_result) do
      {
        level1_a_level2_a: 'hello',
        level1_a_level2_b: 'world',
        level1_b: 'hi!',
      }
    end

    it 'flattens sub-hashes, joining child keys with parent keys ' do
      expect(hash_with_flat_joined_keys).to eq(expected_result)
    end
  end

  describe '#sample' do
    subject(:sample) { hash.sample(n) }

    shared_examples 'it returns a sample' do
      it 'sample hash has length less than or equal to original' do
        expect(sample.length).to be <= hash.length
      end

      it 'returns a hash with key-value pairs from original hash' do
        expect(hash).to include(sample)
      end
    end

    context 'n < 0' do
      let(:n) { -1 }

      it 'raises an error' do
        expect { sample }.to raise_error(ArgumentError, 'negative sample number')
      end
    end

    context 'n = 0' do
      let(:n) { 0 }

      it_behaves_like 'it returns a sample'
    end

    context 'n = 1' do
      let(:n) { 1 }

      it_behaves_like 'it returns a sample'
    end

    context 'n > 1' do
      let(:n) { 50 }

      it_behaves_like 'it returns a sample'
    end
  end
end
