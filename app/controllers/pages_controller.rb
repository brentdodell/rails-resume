class PagesController < ApplicationController
  def about
  end

  def education
  end

  def employment
  end

  def skills
  end

  def interests
  end

  def welcome
    redirect_to(about_path)
  end
end
