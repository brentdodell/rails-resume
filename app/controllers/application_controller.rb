class ApplicationController < ActionController::Base
  def url_options
    options = default_url_options
    options[:theme] = params[:theme]
    options
  end
end
