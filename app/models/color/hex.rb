module Color
  class Hex
    include ActiveModel::Validations

    validates_format_of :hex,
                        with: /\A([a-f0-9]{3}){,2}\z/i,
                        message: 'must be a hexidecimal string'

    attr_reader :hex
    delegate :to_hsl, to: :to_rgb
    delegate :split_complementary, to: :to_hsl, prefix: true
    delegate :contrast_color, to: :to_rgb, prefix: true

    def initialize(hex)
      @hex = hex

      validate!
    end

    def to_a
      hex.scan(/.{2}/)
    end

    def to_rgb
      HexToRGB.(self)
    end

    def split_complementary
      to_hsl_split_complementary.map(&:to_hex)
    end

    def contrast_color
      to_rgb_contrast_color.to_hex
    end
  end
end
