module Color
  class HSLToRGB
    attr_reader :hue, :saturation, :lightness

    def initialize(hsl)
      @hue, @saturation, @lightness = hsl.to_a
    end

    def self.call(hsl)
      new(hsl).rgb
    end

    def rgb
      rgb_ints = fractional_rgb.map { |channel| (channel + m) * 255 }.map(&:round)
      RGB.new(*rgb_ints)
    end

    private

    def c
      (1 - ((2 * lightness) - 1).abs) * saturation
    end

    def x
      c * (1 - (((hue / 60) % 2) - 1).abs)
    end

    def m
      lightness - (c / 2)
    end

    def fractional_rgb
      if 0 <= hue && hue < 60
        [c, x, 0]
      elsif 60 <= hue && hue < 120
        [x, c, 0]
      elsif 120 <= hue && hue < 180
        [0, c, x]
      elsif 180 <= hue && hue < 240
        [0, x, c]
      elsif 240 <= hue && hue < 300
        [x, 0, c]
      elsif 300 <= hue && hue < 360
        [c, 0, x]
      end
    end
  end
end
