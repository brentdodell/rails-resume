module Color
  class HSL
    include ActiveModel::Validations

    validates_inclusion_of :hue, in: 0..360, message: 'must be between 0 and 360'
    validates_inclusion_of :saturation,
                           in: 0..1,
                           message: 'must be between 0.0 and 1.0'
    validates_inclusion_of :lightness,
                           in: 0..1,
                           message: 'must be between 0.0 and 1.0'

    attr_reader :hue, :saturation, :lightness
    delegate :to_hex, to: :to_rgb
    delegate :contrast_color, to: :to_rgb, prefix: true

    def initialize(hue, saturation, lightness)
      @hue = hue
      @saturation = saturation
      @lightness = lightness

      validate!
    end

    def to_a
      [hue, saturation, lightness]
    end

    def to_rgb
      HSLToRGB.(self)
    end

    def split_complementary
      [
        self.class.new(normalize_hue(hue + 150), saturation, lightness),
        self.class.new(normalize_hue(hue - 150), saturation, lightness),
      ]
    end

    def contrast_color
      to_rgb_contrast_color.to_hsl
    end

    private

    def normalize_hue(hue)
      hue % 360
    end
  end
end
