module Color
  class RGBToHex
    attr_reader :red, :green, :blue

    def initialize(rgb)
      @red, @green, @blue = rgb.to_a
    end

    def self.call(rgb)
      new(rgb).hex
    end

    def hex
      hex_strings = [red, green, blue].map { |rgb_int| rgb_int.to_s(16).rjust(2, '0') }
      Hex.new(hex_strings.join)
    end
  end
end
