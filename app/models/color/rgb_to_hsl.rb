module Color
  class RGBToHSL
    attr_reader :red, :green, :blue

    def initialize(rgb)
      @red, @green, @blue = rgb.to_a.map { |channel| channel / 255.0 }
    end

    def self.call(rgb)
      new(rgb).hsl
    end

    def hsl
      HSL.new(hue, saturation, lightness)
    end

    private

    def channels
      [red, green, blue]
    end

    def c_max
      channels.max
    end

    def c_min
      channels.min
    end

    def delta
      c_max - c_min
    end

    def lightness
      (c_max + c_min) / 2.0
    end

    def hue
      if delta == 0
        0
      else
        case c_max
        when red
          60 * (((green - blue) / delta) % 6)
        when green
          60 * (((blue - red) / delta) + 2)
        when blue
          60 * (((red - green) / delta) + 4)
        end
      end
    end

    def saturation
      if delta == 0
        0
      else
        delta / (1 - ((2 * lightness) - 1).abs)
      end
    end
  end
end
