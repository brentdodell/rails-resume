module Color
  class RGB
    include ActiveModel::Validations

    validates_inclusion_of :red, :green, :blue,
                           in: 0..255,
                           message: 'must be between 0 and 255'

    attr_reader :red, :green, :blue
    delegate :split_complementary, to: :to_hsl, prefix: true

    def initialize(red, green, blue)
      @red = red
      @green = green
      @blue = blue

      validate!
    end

    def to_a
      [red, green, blue]
    end

    def to_hsl
      RGBToHSL.(self)
    end

    def to_hex
      RGBToHex.(self)
    end

    def contrast_color
      luminance > 0.5 ? self.class.new(52, 58, 64) : self.class.new(255, 255, 255)
    end

    def split_complementary
      to_hsl_split_complementary.map(&:to_rgb)
    end

    private

    def luminance
      Luminance.new(*to_a).calculate
    end
  end
end
