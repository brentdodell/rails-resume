module Color
  class HexToRGB
    attr_reader :hex_red, :hex_green, :hex_blue

    def initialize(hex)
      @hex_red, @hex_green, @hex_blue = hex.to_a
    end

    def self.call(hex)
      new(hex).rgb
    end

    def rgb
      rgb_ints = [hex_red, hex_green, hex_blue].map(&:hex)
      RGB.new(*rgb_ints)
    end
  end
end
