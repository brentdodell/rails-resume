class Theme
  ORGANIZATION_COLORS = {
    planning_center: {
      accounts: '4e7ba8',
      check_ins: '8a6b97',
      giving: 'fdc539',
      groups: 'ff8252',
      people: '5979b3',
      planning_center: '2565f4',
      registrations: '469c98',
      resources: 'b34b39',
      services: '6e9541',
      music_stand: '339bff',
    },
    bigger_pockets: '1d96f4',
    ynab: '2c97ad',
    igniter_media: 'f07531',
    choctaw_nation: {
      brown: '4f2c1d',
      purple: '5b027a',
    },
  }

  DEFAULT_COLORS = {
    red: '9a334f',
    green: '226666',
    blue: '29506d',
  }

  attr_reader :organization, :app_name

  def initialize(organization: nil, app_name: nil)
    @organization = organization
    @app_name = app_name
  end

  def self.find_by(args)
    new(**args)
  end

  def self.all
    organization_colors + default_colors
  end

  def self.organization_colors
    ORGANIZATION_COLORS.flat_join_keys.map do |name, hex|
      NamedColor.new(
        name: name,
        color: Color::Hex.new(hex)
      )
    end
  end

  def self.default_colors
    DEFAULT_COLORS.map do |name, hex|
      NamedColor.new(
        name: name,
        color: Color::Hex.new(hex)
      )
    end
  end

  def color
    present? ? organization_color : default_color
  end

  def present?
    organization_color.present?
  end

  private

  def organization_color
    return @organization_color if @organization_color

    keys = [organization, app_name].compact.map(&:underscore).map(&:to_sym)
    name = keys.join('-')
    hex_value = ORGANIZATION_COLORS.dig(*keys) if keys.present?
    @organization_color = if hex_value.instance_of?(String)
      NamedColor.new(
        name: name,
        color: Color::Hex.new(hex_value)
      )
    end
  end

  def default_color
    DEFAULT_COLORS.sample.map do |name, hex_value|
      NamedColor.new(
        name: name,
        color: Color::Hex.new(hex_value)
      )
    end.first
  end

  class NamedColor
    attr_reader :name
    delegate_missing_to :color

    def initialize(name:, color:)
      @name = name.to_s.dasherize
      @color = color
    end

    private

    attr_reader :color
  end
end
