class Luminance
  include ActiveModel::Validations

  validates_inclusion_of :red, :green, :blue, in: 0..255, message: 'must be between 0 and 255'

  def initialize(red, green, blue)
    @red = red
    @green = green
    @blue = blue

    validate!
  end

  def calculate
    (0.2126 * channel_luminence(red)) +
      (0.7152 * channel_luminence(green)) +
      (0.0722 * channel_luminence(blue))
  end

  private

  attr_reader :red, :green, :blue

  def channel_luminence(color)
    normalized_color = color / 255.0

    if normalized_color <= 0.03928
      normalized_color / 12.92
    else
      ((normalized_color + 0.055) / 1.055) ** 2.4
    end
  end
end
