module ApplicationHelper
  def body_class
    classes = []

    classes << "appcolor\=#{theme.color.name}"
    classes << controller_name.dasherize
    classes << action_name.dasherize

    classes.join(' ')
  end

  def theme
    organization, app_name = params[:theme]&.split('/')
    Theme.find_by(organization: organization, app_name: app_name)
  end

  def nav_link_to(name = nil, options = nil, html_options = {}, &block)
    css_classes = [html_options[:class]]
    css_classes << 'current' if current_page?(options)
    html_options[:class] = css_classes.compact.join(' ')

    link_to name, options, html_options, &block
  end
end
