# Load Ruby Extensions
require_relative '../lib/extensions'

# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!
