Rails.application.routes.draw do
  constraints ThemeRouteConstraint.new do
    scope "(*theme)" do
      get "/about", to: 'pages#about', as: :about
      get '/education', to: 'pages#education', as: :education
      get '/employment', to: 'pages#employment', as: :employment
      get '/interests', to: 'pages#interests', as: :interests
      get '/skills', to: 'pages#skills', as: :skills

      root to: 'pages#welcome'
    end
  end
end
