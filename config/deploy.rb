# config valid for current version and patch releases of Capistrano
lock '~> 3.11.0'

set :application, 'rails-resume'
set :repo_url, 'git@gitlab.com:brentdodell/rails-resume.git'

append :linked_files, 'config/master.key'
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system'
